#!/usr/bin/env python

import json

with open(file = "test.json", mode = 'r') as read_file:
    print(json.dumps(json.load(read_file), indent = 2))
