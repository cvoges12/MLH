# MLH

### Resources

#### Pandoc

- [filers](https://pandoc.org/filters.html)
- [includes via filters](https://pandoc.org/filters.html#include-files)
- [extensions](https://pandoc.org/MANUAL.html#extensions)
- [raw HTML/TeX](https://pandoc.org/MANUAL.html#raw-htmltex)
- [raw HTML](https://pandoc.org/MANUAL.html#raw-html)
- [raw TeX](https://pandoc.org/MANUAL.html#extension-raw_tex)
- [pdf engines](https://pandoc.org/MANUAL.html#option--pdf-engine)
- [TeX math in html](https://pandoc.org/MANUAL.html#math-rendering-in-html)
- [server](https://github.com/jgm/pandoc/blob/master/doc/pandoc-server.md)
- [API](https://pandoc.org/using-the-pandoc-api.html)
- [markdown html to html instead of pdflatex](https://github.com/wkhtmltopdf/wkhtmltopdf/)
